use std::f32::consts::PI;
use bevy::prelude::{Res, ResMut};
use bevy_egui::{egui, EguiContexts};
use crate::resources::{AIParameters, SimulationParameters};

pub fn ui(mut contexts: EguiContexts, mut ai_parameters: ResMut<AIParameters>, sim_parameters: Res<SimulationParameters>) {
    egui::Window::new("boïds parameters").show(contexts.ctx_mut(), |ui| {
        ui.add(egui::Slider::new(&mut ai_parameters.cohesion_weight, 0.0..=0.01).text("cohesion").show_value(false));
        ui.add(egui::Slider::new(&mut ai_parameters.alignment_weight, 0.0..=0.1).text("alignment").show_value(false));
        ui.add(egui::Slider::new(&mut ai_parameters.avoidance_weight, 0.0..=0.1).text("avoidance").show_value(false));

        ui.add(egui::Slider::new(&mut ai_parameters.fov, 0f32..=PI * 2f32).text("field of view").show_value(false));
        ui.add(egui::Slider::new(&mut ai_parameters.view_radius, 0f32..=sim_parameters.world_size.max_element() / 2f32).text("view distance"));
    });
}