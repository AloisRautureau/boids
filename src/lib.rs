mod boid;
mod components;
mod helper;
mod resources;
mod ui;

use boid::*;
use components::*;
use resources::*;
use ui::*;

use bevy::diagnostic::{FrameTimeDiagnosticsPlugin, LogDiagnosticsPlugin};
use bevy_egui::EguiPlugin;
use bevy::prelude::*;
use bevy::window::PresentMode;
use bevy_obj::*;

pub fn run(population: usize, flocks: usize, world_size: Vec3) {
    let mut app = App::new();

    app.insert_resource(SimulationParameters {
        population,
        flocks,
        world_size,
    })
    .insert_resource(AIParameters::default())
    .insert_resource(ClearColor(Color::BLACK))
    .insert_resource(FixedTime::new_from_secs(1f32 / 60f32));

    // Enable debug assertions if necessary
    #[cfg(debug_assertions)]
    app.add_plugin(LogDiagnosticsPlugin::default())
        .add_plugin(FrameTimeDiagnosticsPlugin::default());

    app.add_plugins(DefaultPlugins.set(WindowPlugin {
        primary_window: Some(Window {
            title: "boïds".into(),
            present_mode: PresentMode::AutoVsync,
            fit_canvas_to_parent: true,
            prevent_default_event_handling: false,
            .. default()
        }),
        .. default()
    }))
        .add_plugin(EguiPlugin)
        .add_plugin(ObjPlugin)
        .add_plugin(BoidsSimulator);

    app.add_system(ui);

    app.run()
}

struct BoidsSimulator;
impl Plugin for BoidsSimulator {
    fn build(&self, app: &mut App) {
        app.add_startup_system(BoidsSimulator::spawn_boids)
            .add_startup_system(BoidsSimulator::setup)
            .add_system(BoidsSimulator::ai.in_schedule(CoreSchedule::FixedUpdate))
            .add_system(BoidsSimulator::physics.in_schedule(CoreSchedule::FixedUpdate));
    }
}
impl BoidsSimulator {
    fn setup(
        mut materials: ResMut<Assets<StandardMaterial>>,
        mut meshes: ResMut<Assets<Mesh>>,
        parameters: Res<SimulationParameters>,
        mut commands: Commands,
    ) {
        // Camera
        commands.spawn((Camera3dBundle {
            transform: Transform::from_xyz(
                -parameters.world_size.x * 0.25,
                parameters.world_size.y * 1.25,
                -parameters.world_size.z * 1.25,
            )
            .looking_at(parameters.world_size / 2f32, Vec3::Y),
            camera: Camera {
                hdr: true,
                ..default()
            },
            ..default()
        },));

        // Lights
        let world_color = Color::rgba_u8(30, 102, 254, 25);
        commands.spawn(SpotLightBundle {
            spot_light: SpotLight {
                intensity: 10000000.0,
                range: parameters.world_size.length() * 100f32,
                radius: parameters.world_size.length(),
                shadows_enabled: true,
                ..default()
            },
            transform: Transform::from_xyz(0f32, parameters.world_size.y, parameters.world_size.z)
                .looking_at(parameters.world_size / 2f32, Vec3::Y),
            ..default()
        });

        // World block
        let world_material = materials.add(StandardMaterial {
            base_color: world_color,
            emissive: world_color,
            alpha_mode: AlphaMode::Add,
            perceptual_roughness: 1f32,

            ..default()
        });
        commands.spawn(PbrBundle {
            mesh: meshes.add(
                shape::Box::new(
                    parameters.world_size.x,
                    parameters.world_size.y,
                    parameters.world_size.z,
                )
                .into(),
            ),
            transform: Transform::from_translation(parameters.world_size / 2f32),
            material: world_material,
            ..default()
        });
    }

    /// Spawns `n` boïds in at random positions in the world, each in a given flock
    fn spawn_boids(
        parameters: Res<SimulationParameters>,
        asset_server: Res<AssetServer>,
        mut materials: ResMut<Assets<StandardMaterial>>,
        mut commands: Commands,
    ) {
        let boid_model = asset_server.load("boid.obj");
        let colours = [
            Color::rgb_u8(254, 100, 11),  // Peach
            Color::rgb_u8(64, 160, 43),   // Green
            Color::rgb_u8(210, 15, 57),   // Red
            Color::rgb_u8(234, 118, 203), // Pink
        ]
        .map(|c| {
            materials.add(StandardMaterial {
                base_color: c,
                emissive: c * 0.25,
                perceptual_roughness: 1f32,
                ..default()
            })
        });

        for i in 0..parameters.population {
            let flock_id = i % parameters.flocks;
            commands.spawn(BoidBundle::new_random(
                FlockId(flock_id),
                boid_model.clone(),
                colours[flock_id].clone(),
                &parameters.world_size,
            ));
        }
    }

    /// Moves each boid by its current velocity, dealing with collisions at the same time
    /// as well as updating the octree
    fn physics(
        sim_parameters: Res<SimulationParameters>,
        ai_parameters: Res<AIParameters>,
        mut query: Query<(&mut Position, &mut Transform, &mut Velocity), With<Boid>>,
    ) {
        for (mut position, mut transform, mut velocity) in &mut query {
            if velocity.0.length() > ai_parameters.max_speed {
                velocity.0 = velocity.0.normalize() * ai_parameters.max_speed;
            }

            let (oob_low, oob_high) = (
                (position.0.cmple(Vec3::splat(15f32)) & velocity.0.cmple(Vec3::ZERO)),
                (position
                    .0
                    .cmpge(sim_parameters.world_size - Vec3::splat(15f32))
                    & velocity.0.cmpge(Vec3::ZERO)),
            );
            velocity.0 += Vec3::new(
                if oob_low.x {
                    1f32
                } else if oob_high.x {
                    -1f32
                } else {
                    0f32
                },
                if oob_low.y {
                    1f32
                } else if oob_high.y {
                    -1f32
                } else {
                    0f32
                },
                if oob_low.z {
                    1f32
                } else if oob_high.z {
                    -1f32
                } else {
                    0f32
                },
            );

            position.0 += velocity.0;
            *transform = transform
                .with_translation(position.0)
                .looking_to(velocity.0, Vec3::Y);
        }
    }

    /// Applies the rules of cohesion, avoidance and alignment to each adjust each boïd's velocity
    fn ai(
        parameters: Res<AIParameters>,
        mut boids: Query<(Entity, &Position, &mut Velocity, &FlockId), With<Boid>>,
    ) {
        const MIN_DIST: f32 = 15f32;

        // Computes the new velocity of each boid
        let mut new_velocities = vec![];
        for (entity, position, velocity, flock) in &boids {
            let mut flock_center = position.0;
            let mut flock_velocity = velocity.0;
            let mut avoidance_correction = Vec3::ZERO;
            let mut _visible_count = 1;
            let mut visible_flock_count = 1;

            for (other_position, other_velocity, other_flock) in
                boids.iter().filter_map(|(e, p, v, f)| {
                    let distance = p.0.distance(position.0);
                    let angle = (p.0 - position.0).angle_between(velocity.0);
                    if entity != e && distance < parameters.view_radius && angle < parameters.fov {
                        Some((p, v, f))
                    } else {
                        None
                    }
                })
            {
                if flock == other_flock {
                    flock_center += other_position.0;
                    flock_velocity += other_velocity.0;
                    visible_flock_count += 1
                }

                if position.0.distance(other_position.0) < MIN_DIST {
                    avoidance_correction += position.0 - other_position.0;
                }
                _visible_count += 1;
            }

            let avoidance_vector = avoidance_correction * parameters.avoidance_weight;
            flock_center /= visible_flock_count as f32;
            flock_velocity /= visible_flock_count as f32;

            let cohesion_vector = (flock_center - position.0) * parameters.cohesion_weight;
            let alignment_vector = (flock_velocity - velocity.0) * parameters.alignment_weight;

            new_velocities.push((
                entity,
                cohesion_vector + avoidance_vector + alignment_vector,
            ))
        }

        // Then updates them
        for (entity, new_velocity) in new_velocities {
            if let Ok((_, _, mut velocity, _)) = boids.get_mut(entity) {
                velocity.0 += new_velocity;
            }
        }
    }
}
