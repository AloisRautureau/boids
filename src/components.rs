use bevy::prelude::*;

#[derive(Component)]
pub struct Boid;

#[repr(transparent)]
#[derive(Component, PartialEq, Debug)]
pub struct Position(pub Vec3);

#[repr(transparent)]
#[derive(Component, PartialEq, Debug)]
pub struct Velocity(pub Vec3);

#[repr(transparent)]
#[derive(Component, Ord, PartialOrd, Eq, PartialEq, Debug)]
pub struct FlockId(pub usize);
