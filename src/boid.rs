use crate::components::*;
use crate::helper::random_vec3;
use bevy::prelude::*;

#[derive(Bundle)]
pub struct BoidBundle {
    _marker: Boid,
    position: Position,
    velocity: Velocity,
    flock: FlockId,

    model: PbrBundle,
}
impl BoidBundle {
    /// Creates a new boïd at a random position within the world
    pub fn new_random(
        flock: FlockId,
        mesh: Handle<Mesh>,
        material: Handle<StandardMaterial>,
        world_size: &Vec3,
    ) -> BoidBundle {
        let position = Position(random_vec3(&Vec3::ZERO, world_size));
        let velocity = Velocity(random_vec3(&(-Vec3::splat(5f32)), &Vec3::splat(5f32)));

        let model = PbrBundle {
            mesh,
            material,
            transform: Transform::from_translation(position.0)
                .looking_to(velocity.0, Vec3::Y)
                .with_scale(Vec3::splat(5f32)),
            ..default()
        };

        BoidBundle {
            _marker: Boid,
            position,
            velocity,
            flock,

            model,
        }
    }
}
