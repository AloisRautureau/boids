use bevy::prelude::*;

#[derive(Resource)]
pub struct SimulationParameters {
    pub population: usize,
    pub flocks: usize,
    pub world_size: Vec3,
}

#[derive(Resource)]
pub struct AIParameters {
    pub alignment_weight: f32,
    pub cohesion_weight: f32,
    pub avoidance_weight: f32,

    pub fov: f32,
    pub view_radius: f32,

    pub max_speed: f32,
}
impl Default for AIParameters {
    fn default() -> Self {
        AIParameters {
            alignment_weight: 0.05,
            cohesion_weight: 0.005,
            avoidance_weight: 0.05,

            fov: std::f32::consts::PI / 2f32,
            view_radius: 50f32,

            max_speed: 15f32,
        }
    }
}
