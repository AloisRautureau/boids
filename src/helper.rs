use bevy::prelude::Vec3;
use rand::Rng;

/// Generates a random vector within the given bounds
pub fn random_vec3(min: &Vec3, max: &Vec3) -> Vec3 {
    let mut rng = rand::thread_rng();
    let mut gen = |min: f32, max: f32| rng.gen_range(min..max);
    Vec3::new(gen(min.x, max.x), gen(min.y, max.y), gen(min.z, max.z))
}
