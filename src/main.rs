use bevy::math::Vec3;
use boids::run;

fn main() {
    run(400, 4, Vec3::splat(500f32))
}
